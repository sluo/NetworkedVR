#include "TempCoherence.h"
#include <iostream>
#include <fstream>
#include <string>

texture <unsigned char, cudaTextureType3D, cudaReadModeElementType> prevTexRef;
texture <unsigned char, cudaTextureType3D, cudaReadModeElementType> currTexRef;
texture <unsigned char, cudaTextureType3D, cudaReadModeElementType> nextTexRef;
texture <unsigned char, cudaTextureType3D, cudaReadModeElementType> exactTexRef;


texture <unsigned char, cudaTextureType2D, cudaReadModeElementType> tfRef;



TempCoherence::TempCoherence(int screenWidth, int screenHeight, VolumeDataset &volume, NetworkManager *networkManager)
{
	blockRes = BLOCK_RES;
	alpha = 6;

	netManager = networkManager;

	histogram = new FrequencyHistogram();

	numXBlocks = glm::ceil((float)volume.xRes / (float)blockRes);
	numYBlocks = glm::ceil((float)volume.yRes / (float)blockRes);
	numZBlocks = glm::ceil((float)volume.zRes / (float)blockRes);
	numBlocks = numXBlocks * numYBlocks * numZBlocks;

	textureSize = volume.xRes * volume.yRes * volume.zRes * volume.bytesPerElement;
	prevTexture3D = volume.GenerateTexture();
	currTexture3D = volume.GenerateTexture();
	nextTexture3D = volume.GenerateTexture();
	exactTexture3D = volume.GenerateTexture();	
	
	/*---------------------------------*/

	/*glGenTextures(1, &tf2D);
	glBindTexture(GL_TEXTURE_1D, tf2D);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage1D(GL_TEXTURE_1D, 0,  GL_RGBA, 256,0, GL_RGBA, GL_FLOAT, 0);
	glBindTexture(GL_TEXTURE_1D, 0);


	 std::vector< glm::vec4 >  tfdata;

	glm::vec4 init_value;

	init_value.x = 0.5;
	init_value.y = 0.5;
	init_value.z = 0.5;
	init_value.w = 0.5;
	for (int i = 0; i < 256;i++)
	tfdata.push_back(init_value);
	

	glBindTexture(GL_TEXTURE_1D, tf2D);
	glTexSubImage1D(GL_TEXTURE_1D,  0, 0,  256, GL_RGBA, GL_FLOAT, &tfdata[0]);
	glBindTexture(GL_TEXTURE_1D, 0);
	*/
	/*---------------------------------*/	
		
	for (int i=0; i<4; i++)
		cudaResources.push_back(cudaGraphicsResource_t());

	HANDLE_ERROR( cudaMalloc((void**)&cudaBlockFlags, numBlocks * sizeof(bool)) );
	hostBlockFlags = new bool[numBlocks];

	epsilon =0.0f;
}

__global__ void CudaPredict(int numVoxels, int xRes, int yRes, int zRes, cudaSurfaceObject_t surface)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	if (tid < numVoxels)
	{
		int z = tid / (xRes * yRes);
		int remainder = tid % (xRes * yRes);

		int y = remainder / xRes;

		int x = remainder % xRes;

		unsigned char prevVal, currVal, nextVal;

		prevVal = tex3D(prevTexRef, x, y, z);
		currVal = tex3D(currTexRef, x, y, z);

		int temp = (EXTRAP_CONST * currVal) - prevVal;
		nextVal = (unsigned char)glm::clamp(temp, 0, 255);

		surf3Dwrite(nextVal, surface, x, y, z);
	}
}

__global__ void CudaBlockCompare(float epsilon, int numBlocks, int blockRes, int numXBlocks, int numYBlocks, int numZBlocks, int volumeXRes, int volumeYRes, int volumeZRes, bool *cudaBlockFlags, cudaSurfaceObject_t surface, int bKernelRadius)
{
	int tid = threadIdx.x + (blockIdx.x * blockDim.x);

	/* Trasfer Function Blue*/
	//float Alpha[256] ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.000816659,0.0186347,0.0256082,0.0219608,0.0183133,0.0146658,0.0110184,0.00737092,0.00372345,0.0,0.000329377,0.000665762,0.00100215,0.00133853,0.00167492,0.0020113,0.00234769,0.00268407,0.00302046,0.00335684,0.00369322,0.00402961,0.00436599,0.00470238,0.00503876,0.00537515,0.00571153,0.00604792,0.0063843,0.00672069,0.00705707,0.00739346,0.00772984,0.00806623,0.00840261,0.008739,0.00907538,0.00941177,0.00974815,0.0100845,0.0104209,0.0107573,0.0110937,0.0114301,0.0117681,0.0124114,0.0130547,0.013698,0.0143414,0.0149847,0.015628,0.0162714,0.0169147,0.017558,0.0182013,0.0188447,0.019488,0.0201313,0.0207747,0.021418,0.0220613,0.0227046,0.023348,0.0239913,0.0246346,0.0252779,0.0259213,0.0265646,0.0272079,0.0278513,0.0284946,0.0291379,0.0297812,0.0304246,0.0310679,0.0317112,0.0323546,0.0329979,0.0336412,0.0342845,0.0349279,0.0355712,0.0362145,0.0368579,0.0375012,0.0381445,0.0387878,0.0394312,0.0400745,0.0407178,0.0413611,0.0420045,0.0426478,0.0432911,0.0439345,0.0445778,0.0452211,0.0458644,0.0465078,0.0471511,0.0477944,0.0484378,0.0490811,0.0497244,0.0503677,0.0510111,0.0516544,0.0522977,0.0529411,0.0535844,0.0542277,0.054871,0.0555144,0.0561577,0.056801,0.0574444,0.0580877,0.058731,0.0593743,0.0600177,0.060661,0.0613043,0.0619477,0.062591,0.0614466,0.059739,0.0580314,0.0563238,0.0546162,0.0529086,0.0512011,0.0494935,0.0477859,0.0460783,0.0443707,0.0426631,0.0409555,0.0392479,0.0375403,0.0358327,0.0341251,0.0324176,0.03071,0.0290024,0.0272948,0.0255872,0.0238796,0.022172,0.0204644,0.0187568,0.0170492,0.0153416,0.013634,0.0119265,0.0102189,0.00851128,0.00680368,0.00509609,0.0033885,0.00168091,0,0,0,0,0,0,0,0,0,0,0,0,0,0.00673616,0.0176046,0.028473,0.0393414,0.0502098,0.0610782,0.0719466,0.0828151,0.0936835,0.104552,0.11542,0.126289,0.137157,0.148026,0.158894,0.169762,0.180631,0.191499,0.202368,0.213236,0.224104,0.234973,0.245841,0.25671,0.267578,0.278446,0.289315,0.300183,0.311052,0.321573,0.321723,0.321872,0.322021,0.322171,0.32232,0.322469,0.322619,0.322768,0.322917,0.323067,0.323216,0.323365,0.323514,0.323664,0.323813,0.323962,0.324112,0.324261,0.32441,0.32456,0.324709,0.324858,0.325008,0.325157,0.325306,0.325456,0.300757,0.268543,0.236329,0.204115,0.1719,0.139686,0.107472,0.075258,0.0430438,0.0108296,0,0,0};
	
	/*Transfer Function Colors*/
	//float Alpha[256] = { 0.0, 0.00575163, 0.0115033, 0.0172549, 0.0230065, 0.0287582, 0.0345098, 0.0402614, 0.0460131, 0.0517647, 0.0575163, 0.063268, 0.0690196, 0.0747712, 0.0805229, 0.0862745, 0.0813072, 0.0763399, 0.0713725, 0.0664052, 0.0614379, 0.0564706, 0.0515033, 0.0465359, 0.0415686, 0.0366013, 0.031634, 0.0266667, 0.0216993, 0.016732, 0.0117647, 0.0109804, 0.0101961, 0.00941176, 0.00862745, 0.00784314, 0.00705882, 0.00627451, 0.0054902, 0.00470588, 0.00392157, 0.00313726, 0.00235294, 0.00156863, 0.000784313, 0, 0.000784314, 0.00156863, 0.00235294, 0.00313726, 0.00392157, 0.00470588, 0.0054902, 0.00627451, 0.00705882, 0.00784314, 0.00862745, 0.00941177, 0.0101961, 0.0109804, 0.0117647, 0.0130719, 0.0143791, 0.0156863, 0.0169935, 0.0183007, 0.0196078, 0.020915, 0.0222222, 0.0235294, 0.0248366, 0.0261438, 0.027451, 0.0287582, 0.0300654, 0.0313726, 0.0321569, 0.0329412, 0.0337255, 0.0345098, 0.0352941, 0.0360784, 0.0368627, 0.0376471, 0.0384314, 0.0392157, 0.04, 0.0407843, 0.0415686, 0.0423529, 0.0431373, 0.0457516, 0.048366, 0.0509804, 0.0535948, 0.0562092, 0.0588235, 0.0614379, 0.0640523, 0.0666667, 0.069281, 0.0718954, 0.0745098, 0.0771242, 0.0797386, 0.0823529, 0.0839216, 0.0854902, 0.0870588, 0.0886275, 0.0901961, 0.0917647, 0.0933333, 0.094902, 0.0964706, 0.0980392, 0.0996078, 0.101176, 0.102745, 0.104314, 0.105882, 0.109804, 0.113725, 0.117647, 0.121569, 0.12549, 0.129412, 0.133333, 0.137255, 0.141176, 0.145098, 0.14902, 0.152941, 0.156863, 0.160784, 0.164706, 0.166013, 0.16732, 0.168627, 0.169935, 0.171242, 0.172549, 0.173856, 0.175163, 0.176471, 0.177778, 0.179085, 0.180392, 0.181699, 0.183007, 0.184314, 0.186405, 0.188497, 0.190588, 0.19268, 0.194771, 0.196863, 0.198954, 0.201046, 0.203137, 0.205229, 0.20732, 0.209412, 0.211503, 0.213595, 0.215686, 0.220131, 0.224575, 0.22902, 0.233464, 0.237908, 0.242353, 0.246797, 0.251242, 0.255686, 0.260131, 0.264575, 0.26902, 0.273464, 0.277909, 0.282353, 0.281046, 0.279739, 0.278431, 0.277124, 0.275817, 0.27451, 0.273203, 0.271895, 0.270588, 0.269281, 0.267974, 0.266667, 0.265359, 0.264052, 0.262745, 0.26719, 0.271634, 0.276078, 0.280523, 0.284967, 0.289412, 0.293856, 0.298301, 0.302745, 0.30719, 0.311634, 0.316078, 0.320523, 0.324967, 0.329412, 0.328105, 0.326797, 0.32549, 0.324183, 0.322876, 0.321569, 0.320261, 0.318954, 0.317647, 0.31634, 0.315033, 0.313726, 0.312418, 0.311111, 0.309804, 0.32366, 0.337516, 0.351373, 0.365229, 0.379085, 0.392941, 0.406797, 0.420654, 0.43451, 0.448366, 0.462222, 0.476078, 0.489935, 0.503791, 0.517647, 0.483137, 0.448627, 0.414118, 0.379608, 0.345098, 0.310588, 0.276078, 0.241569, 0.207059, 0.172549, 0.138039, 0.103529, 0.0690196, 0.0345098, 0.0 };
	
	
	if (tid < numBlocks)
	{
		int z = tid / (numXBlocks * numYBlocks);
		int remainder = tid % (numXBlocks * numYBlocks);

		int y = remainder / numXBlocks;

		int x = remainder % numXBlocks;

		int xMin = x * blockRes;
		int yMin = y * blockRes;
		int zMin = z * blockRes;

		float top, bottom;
		top = bottom = 0.0f;

		// mean value
		float m = 0.0;
		int numVoxels = blockRes*blockRes*blockRes;
		unsigned char maxim=0;
		unsigned char rn = 0.0;
		unsigned char rd = 0.0;
		unsigned char Autocorrelation = 0.0;
		unsigned char W_k = 0.0;
		unsigned char a, b = 0.0;
		a = 0.0;
		b = 2.0;

		// new for computing the mean value m among all the densities inside a block.
		//----------------------------------------------------------------------------
	/*	for (int k = 0; k<blockRes; k += CHECK_STRIDE)
		{
			for (int j = 0; j<blockRes; j += CHECK_STRIDE)
			{
				for (int i = 0; i<blockRes; i += CHECK_STRIDE)
				{
					if ((xMin + i) >= volumeXRes || (yMin + j) >= volumeYRes || (zMin + k) >= volumeZRes)
						continue;

					unsigned char exactVal, sqdiff;
					
					m+=exactVal = tex3D(exactTexRef, xMin + i, yMin + j, zMin + k);	

					//finding the maxim of all the neighbors
					sqdiff = (tex3D(exactTexRef, xMin + i + 1, yMin + j, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(tex3D(exactTexRef, xMin + i + 1, yMin + j, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k));
					if (sqdiff > maxim)
						maxim = sqdiff;

					sqdiff = (tex3D(exactTexRef, xMin + i -1, yMin + j, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(tex3D(exactTexRef, xMin + i - 1, yMin + j, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k));
					if (sqdiff > maxim)
						maxim = sqdiff;


					sqdiff = (tex3D(exactTexRef, xMin + i, yMin + j+1, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(tex3D(exactTexRef, xMin + i, yMin + j+1, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k));
					if (sqdiff > maxim)
						maxim = sqdiff;


					sqdiff = (tex3D(exactTexRef, xMin + i , yMin + j-1, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(tex3D(exactTexRef, xMin + i , yMin + j-1, zMin + k) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k));
					if (sqdiff > maxim)
						maxim = sqdiff;
					sqdiff = (tex3D(exactTexRef, xMin + i, yMin + j, zMin + k+1) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(tex3D(exactTexRef, xMin + i, yMin + j, zMin + k+1) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k));
					if (sqdiff > maxim)
						maxim = sqdiff;
					sqdiff = (tex3D(exactTexRef, xMin + i , yMin + j, zMin + k-1) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(tex3D(exactTexRef, xMin + i, yMin + j, zMin + k-1) - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k));
					if (sqdiff > maxim)
						maxim = sqdiff;

				//	unsigned char maxdiff = (maxim - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))*(maxim - tex3D(exactTexRef, xMin + i, yMin + j, zMin + k))
				   
				}
			}
	}
			*/
	//	 m = m / numVoxels;
		//---------------------------------------------------------------------------
		int counterNull=0;

		for (int k=0; k<blockRes; k+=CHECK_STRIDE)
		{
			for (int j=0; j<blockRes; j+=CHECK_STRIDE)
			{
				for (int i=0; i<blockRes; i+=CHECK_STRIDE)
				{
					if ((xMin + i) >= volumeXRes || (yMin + j) >= volumeYRes || (zMin + k) >= volumeZRes)
						continue;

					unsigned char predictVal, exactVal, exactVal2;
				
					surf3Dread(&predictVal, surface, xMin + i, yMin + j, zMin + k);
					exactVal = tex3D(exactTexRef, xMin + i, yMin + j, zMin + k);
					
					int diff =  exactVal - predictVal;
					if (exactVal==0)
					{
					
						counterNull++;

					}

					//int diff = Alpha[exactVal + predictVal] * exactVal - predictVal;
					// compute the square diff and then the autocorrelation
			/*		for (int c = 0; c < blockRes; c += CHECK_STRIDE)
					{
						for (int b = 0; b < blockRes; b += CHECK_STRIDE)
						{
							for (int a = 0; a < blockRes; a += CHECK_STRIDE)
							{
								if ((xMin + a) >= volumeXRes || (yMin + b) >= volumeYRes || (zMin + c) >= volumeZRes)
									continue;


								exactVal2 = tex3D(exactTexRef, xMin + a, yMin + b, zMin + c);
								
								float dist2 = glm::distance2(glm::vec3(i, j,k), glm::vec3(a,b,c));
							
								if (dist2 < bKernelRadius * bKernelRadius)
								{

									rn += (exactVal2 - m)*(exactVal - m);
									rd += (exactVal - m)*(exactVal - m);

								}				
							}
						}
					}
				*/	
					top += diff * diff;
				}
			}
		}

	//	Autocorrelation = rn / rd;

		W_k = 1.0 + a*(1.0 - Autocorrelation) + b* maxim;
		
		int numPerAxis = blockRes / CHECK_STRIDE;
		bottom = numPerAxis * numPerAxis * numPerAxis;

		float similar = glm::sqrt((/*W_k**/top) / bottom);

		if (/*(similar > epsilon) || */(counterNull!= blockRes*blockRes*blockRes))
		{
			cudaBlockFlags[tid] = true;

			for (int k=0; k<blockRes; k+=CHECK_STRIDE)
				for (int j=0; j<blockRes; j+=CHECK_STRIDE)
					for (int i=0; i<blockRes; i+=CHECK_STRIDE)
					{
						if ((xMin + i) >= volumeXRes || (yMin + j) >= volumeYRes || (zMin + k) >= volumeZRes)
						
							continue;

						unsigned char exactVal = tex3D(exactTexRef, xMin + i, yMin + j, zMin + k);

						surf3Dwrite(exactVal, surface, xMin + i, yMin + j, zMin + k);
					}
		}
	}
}

void TempCoherence::MapTexturesToCuda()
{
	HANDLE_ERROR( cudaGraphicsGLRegisterImage(&cudaResources[0], prevTexture3D, GL_TEXTURE_3D, cudaGraphicsRegisterFlagsNone) );
	HANDLE_ERROR( cudaGraphicsGLRegisterImage(&cudaResources[1], currTexture3D, GL_TEXTURE_3D, cudaGraphicsRegisterFlagsNone) );
	HANDLE_ERROR( cudaGraphicsGLRegisterImage(&cudaResources[2], nextTexture3D, GL_TEXTURE_3D, cudaGraphicsRegisterFlagsSurfaceLoadStore) );
	HANDLE_ERROR( cudaGraphicsGLRegisterImage(&cudaResources[3], exactTexture3D, GL_TEXTURE_3D, cudaGraphicsRegisterFlagsNone) );
	//HANDLE_ERROR( cudaGraphicsGLRegisterImage(&cudaResources[4], tf2D          , GL_TEXTURE_1D, cudaGraphicsRegisterFlagsNone) );
	HANDLE_ERROR( cudaGraphicsMapResources(4, &cudaResources[0]) );

	prevArry = 0;	
	HANDLE_ERROR( cudaGraphicsSubResourceGetMappedArray(&prevArry, cudaResources[0], 0, 0) ); 
	HANDLE_ERROR( cudaBindTextureToArray(prevTexRef, prevArry) );

	currArry = 0;	
	HANDLE_ERROR( cudaGraphicsSubResourceGetMappedArray(&currArry, cudaResources[1], 0, 0) ); 
	HANDLE_ERROR( cudaBindTextureToArray(currTexRef, currArry) );

	nextArry = 0;	
	HANDLE_ERROR( cudaGraphicsSubResourceGetMappedArray(&nextArry, cudaResources[2], 0, 0) );

	exactArry = 0;	
	HANDLE_ERROR( cudaGraphicsSubResourceGetMappedArray(&exactArry, cudaResources[3], 0, 0) ); 
	HANDLE_ERROR( cudaBindTextureToArray(exactTexRef, exactArry) );

	//tfArry = 0;
	//HANDLE_ERROR( cudaGraphicsSubResourceGetMappedArray(&tfArry,    cudaResources[4], 0, 0) );
	//HANDLE_ERROR( cudaBindTextureToArray(tfRef, tfArry) );
}


void TempCoherence::UnmapTextures()
{
	// Unbind and unmap, must be done before OpenGL uses texture memory again
	HANDLE_ERROR( cudaUnbindTexture(prevTexRef) );
	HANDLE_ERROR( cudaUnbindTexture(currTexRef) );
	HANDLE_ERROR( cudaUnbindTexture(nextTexRef) );
	HANDLE_ERROR( cudaUnbindTexture(exactTexRef) );
	//HANDLE_ERROR( cudaUnbindTexture(tfRef) );

	HANDLE_ERROR( cudaGraphicsUnmapResources(4, &cudaResources[0]) );

	HANDLE_ERROR( cudaGraphicsUnregisterResource(cudaResources[0]) );
	HANDLE_ERROR( cudaGraphicsUnregisterResource(cudaResources[1]) );
	HANDLE_ERROR( cudaGraphicsUnregisterResource(cudaResources[2]) );
	HANDLE_ERROR( cudaGraphicsUnregisterResource(cudaResources[3]) );
	//HANDLE_ERROR( cudaGraphicsUnregisterResource(cudaResources[4]));
}

void TempCoherence::GPUPredict(VolumeDataset &volume)
{
	cudaResourceDesc wdsc;
	wdsc.resType = cudaResourceTypeArray;
	wdsc.res.array.array = nextArry;
	cudaSurfaceObject_t writeSurface;
	HANDLE_ERROR( cudaCreateSurfaceObject(&writeSurface, &wdsc) );

	CudaPredict <<<(volume.numVoxels + 255) / 256, 256>>>(volume.numVoxels, volume.xRes, volume.yRes, volume.zRes, writeSurface);

	HANDLE_ERROR( cudaDeviceSynchronize() );

	CudaBlockCompare << <(numBlocks + 255) / 256, 256 >> >(epsilon, numBlocks, blockRes, numXBlocks, numYBlocks, numZBlocks, volume.xRes, volume.yRes, volume.zRes, cudaBlockFlags, writeSurface,B_SQ_DIFF_RADIUS );

	HANDLE_ERROR( cudaDeviceSynchronize() );
}

GLuint TempCoherence::TemporalCoherence(VolumeDataset &volume, int currentTimestep)
{
	
	std::ofstream myfile("Extrapolation Ratio.txt", std::ios::app);
	if (myfile.is_open())
	{
		
	numBlocksCopied = numBlocksExtrapolated = 0;
	atomicNumBlocksCopied = 0;

	GLuint temp = prevTexture3D;
	prevTexture3D = currTexture3D;
	currTexture3D = nextTexture3D;
	nextTexture3D = temp;

	HANDLE_ERROR( cudaMemset(cudaBlockFlags, 0, numBlocks * sizeof(bool)) );

	if (currentTimestep < 2)
	{
		glBindTexture(GL_TEXTURE_3D, nextTexture3D);
		glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, volume.xRes, volume.yRes, volume.zRes, GL_RED, GL_UNSIGNED_BYTE, volume.currMemblock);
		glBindTexture(GL_TEXTURE_3D, 0);
		
		netManager->SendState(numXBlocks, numYBlocks, numZBlocks, blockRes);		
	}
	else
	{
		glBindTexture(GL_TEXTURE_3D, exactTexture3D);
		glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, volume.xRes, volume.yRes, volume.zRes, GL_RED, GL_UNSIGNED_BYTE, volume.currMemblock);
		glBindTexture(GL_TEXTURE_3D, 0);

		MapTexturesToCuda();
		GPUPredict(volume);
		//		histogram->Update(currentTimestep, volume, currTexture3D);
		UnmapTextures();

		HANDLE_ERROR(cudaMemcpy(hostBlockFlags, cudaBlockFlags, numBlocks * sizeof(bool), cudaMemcpyDeviceToHost));

		int blockID = 0;
		for (int k = 0; k < numZBlocks; k++)
		for (int j = 0; j < numYBlocks; j++)
		for (int i = 0; i < numXBlocks; i++)
		{
			if (hostBlockFlags[blockID])
			{
				numBlocksCopied++;
				netManager->SendBlock(i, j, k, blockRes);
			}
			else
				numBlocksExtrapolated++;

			blockID++;
		}

		//	std::cout << "copied: " << numBlocksCopied << "   -   extrapolated: " << numBlocksExtrapolated << std::endl;
		
		myfile << (float)numBlocksExtrapolated / (float)(numBlocksExtrapolated + numBlocksCopied) << std::endl;
		//counterPrint++;		
				   
	}
	
	return nextTexture3D;
	myfile.close();
	}
	else std::cout << "Unable to open file";
	return 0;
}


GLuint TempCoherence::GenerateTexture(VolumeDataset &volume)
{
	GLuint tex;

	glEnable(GL_TEXTURE_3D);
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_3D, tex);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glTexImage3D(GL_TEXTURE_3D, 0, GL_R8, volume.xRes, volume.yRes, volume.zRes, 0,  GL_RED, GL_UNSIGNED_BYTE, volume.currMemblock);

	glBindTexture(GL_TEXTURE_3D, 0);

	return tex;


}


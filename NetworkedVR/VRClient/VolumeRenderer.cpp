#include "VolumeRenderer.h"



void VolumeRenderer::Init(int screenWidth, int screenHeight, const std::string transferFunctionFile)
{
	glEnable(GL_DEPTH_TEST);

	camera.Init(screenWidth, screenHeight);
	ShaderManager::Init();

	oldTime = clock();

	numFrames = 0;
	frameTime = 0.0f;

	raycaster = new Raycaster(screenWidth, screenHeight, volume);

	transferFunction.Init(transferFunctionFile.c_str(), volume);
//	imageProcessor.Init(screenWidth, screenHeight);
}




void VolumeRenderer::Update()
{
	
	camera.Update();

	//if (volume.timesteps > 0)
	//{
		volume.CopyToTexture();
		clock_t currentTime = clock();
		float time = (currentTime - oldTime) / (float)CLOCKS_PER_SEC;


		if (time > volume.timePerFrame)
		{
			if (currentTimestep < volume.timesteps - 1)
				currentTimestep++;
			else
				currentTimestep = 0;

			oldTime = currentTime;
//		imageProcessor.Begin();

		GLuint shaderProgramID = ShaderManager::UseShader(TFShader);
		raycaster->Raycast(transferFunction, shaderProgramID, camera, volume.currTexture3D);

//		imageProcessor.GetAutoCorrelation();
//		imageProcessor.WriteToTexture();
//		imageProcessor.End();

		frameTime += time;
	}
		numFrames++;


	//	if (currentTimestep == 20)
		//{
		//	float avgFrameTime = frameTime / (float)numFrames;
		//	std::cout << "Avg Frame Time: " << avgFrameTime << std::endl;
			//std::cout << "Avg Extrap: " << extrapRatio / 500.0f << std::endl;
		//	getchar();
		//}
}


